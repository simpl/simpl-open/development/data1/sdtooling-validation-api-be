# SDTooling Validation Be changelog

---
## v1.4.0 (Sprint 10)

#### Security patches:
- shacl dependency com.google.protobuf/protobuf-java from 3.24.3 to last safe version 4.29.2
