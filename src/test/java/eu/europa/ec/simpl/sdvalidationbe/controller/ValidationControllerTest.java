package eu.europa.ec.simpl.sdvalidationbe.controller;

import eu.europa.ec.simpl.sdvalidationbe.TestSupport;
import eu.europa.ec.simpl.sdvalidationbe.logging.ValidationApiConstants;
import eu.europa.ec.simpl.sdvalidationbe.service.ValidationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.InputStream;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ValidationController.class)
class ValidationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private ValidationService validationService;

    @InjectMocks
    private ValidationController validationController;

    @MockitoBean
    private HealthEndpoint healthEndpoint;

    private static Stream<Arguments> ValidateTTLParameters() {
        return Stream.of(
                // valid: result must contain nothing
                Arguments.of("simpl", "data-offering-valid.ttl", ""),
                // not valid: result must contain @graph
                Arguments.of("simpl", "data-offering-invalid.ttl", "@graph")
        );
    }

    @ParameterizedTest
    @MethodSource("ValidateTTLParameters")
    void testValidateTTLUriNotFound(String ecosystem, String dataTTLFile, String resultContainedString) throws Exception {
        try (
                InputStream ttlStream = TestSupport.getResourceAsStream("ValidateTTL-tests/" + dataTTLFile);
                InputStream shapeStream = TestSupport.getResourceAsStream("ValidateTTL-tests/data-offeringShape.ttl")
        ) {
            MockMultipartFile ttlFile = new MockMultipartFile(
                    "ttl-file", "", MediaType.TEXT_PLAIN_VALUE, ttlStream
            );
            MockMultipartFile shapeFile = new MockMultipartFile(
                    "shape-file", "", MediaType.TEXT_PLAIN_VALUE, shapeStream
            );
            mockMvc.perform(
                            multipart("/validate-ttl")
                                    .file(ttlFile)
                                    .file(shapeFile)
                                    .param("ecosystem", ecosystem)
                                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    )
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(resultContainedString)));
        }
    }

    private static Stream<Arguments> ValidateJSONParameters() {
        return Stream.of(
                // valid: result must contain nothing
                Arguments.of("simpl", "sd-data-offering-enriched.json", ""),
                // not valid: result must contain @graph
                Arguments.of("simpl", "sd-data-offering-enriched-invalid.json", "@graph")
        );
    }

    @ParameterizedTest
    @MethodSource("ValidateJSONParameters")
    void testValidateJSONUriNotFound(String ecosystem, String dataTTLFile, String resultContainedString) throws Exception {
        try (
                InputStream jsonStream = TestSupport.getResourceAsStream("ValidateJSON-tests/" + dataTTLFile);
                InputStream shapeStream = TestSupport.getResourceAsStream("ValidateJSON-tests/data-offeringShape.ttl")
        ) {
            MockMultipartFile jsonldFile = new MockMultipartFile(
                    "jsonld-file", "", MediaType.TEXT_PLAIN_VALUE, jsonStream
            );
            MockMultipartFile shapeFile = new MockMultipartFile(
                    "shape-file", "", MediaType.TEXT_PLAIN_VALUE, shapeStream
            );

            mockMvc.perform(
                            multipart("/validate-jsonld")
                                    .file(jsonldFile)
                                    .file(shapeFile)
                                    .param("ecosystem", ecosystem)
                                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    )
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(resultContainedString)));
        }
    }

    @Test
    void testValidateJSONInvalidEcosystem() throws Exception {
        MockitoAnnotations.openMocks(this);
        MockMvc currrentMockMvc = MockMvcBuilders.standaloneSetup(validationController).build();

        when(validationService.isValidEcosystem("invalid_ecosystem")).thenReturn(false);

        MockMultipartFile ttlFile = new MockMultipartFile("jsonld-file", "file.json", "text/plain", "jsonld content".getBytes());
        MockMultipartFile shapeFile = new MockMultipartFile("shape-file", "file.shape", "text/plain", "shape content".getBytes());


        currrentMockMvc.perform(multipart("/validate-jsonld")
                        .file(ttlFile)
                        .file(shapeFile)
                        .param("ecosystem", "invalid_ecosystem"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(ValidationApiConstants.INVALID_ECOSYSTEM.getMessage()));
    }

    @Test
    void testValidateTTLInvalidEcosystem() throws Exception {
        MockitoAnnotations.openMocks(this);
        MockMvc currrentMockMvc = MockMvcBuilders.standaloneSetup(validationController).build();

        when(validationService.isValidEcosystem("invalid_ecosystem")).thenReturn(false);

        MockMultipartFile ttlFile = new MockMultipartFile("ttl-file", "file.ttl", "text/plain", "ttl content".getBytes());
        MockMultipartFile shapeFile = new MockMultipartFile("shape-file", "file.shape", "text/plain", "shape content".getBytes());


        currrentMockMvc.perform(multipart("/validate-ttl")
                        .file(ttlFile)
                        .file(shapeFile)
                        .param("ecosystem", "invalid_ecosystem"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(ValidationApiConstants.INVALID_ECOSYSTEM.getMessage()));
    }

}
