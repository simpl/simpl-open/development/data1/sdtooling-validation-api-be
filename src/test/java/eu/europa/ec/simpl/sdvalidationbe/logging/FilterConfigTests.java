package eu.europa.ec.simpl.sdvalidationbe.logging;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration(classes = {FilterConfig.class})
@ExtendWith(SpringExtension.class)
class FilterConfigTests {
    @Autowired
    private FilterConfig filterConfig;

    /**
     * Method under test: {@link FilterConfig#loggingFilter()}
     */
    @Test
    void testLoggingFilter() {
        // Arrange and Act
        FilterRegistrationBean<RequestSizeFilter> actualLoggingFilterResult = filterConfig.loggingFilter();

        // Assert
        Collection<String> servletNames = actualLoggingFilterResult.getServletNames();
        assertInstanceOf(Set.class, servletNames, "Verify that servletNames is an instance of Set");

        Collection<ServletRegistrationBean<?>> servletRegistrationBeans = actualLoggingFilterResult
                .getServletRegistrationBeans();
        assertInstanceOf(Set.class, servletRegistrationBeans, "Verify that servletRegistrationBeans is an instance of Set");

        Collection<String> urlPatterns = actualLoggingFilterResult.getUrlPatterns();
        assertEquals(1, urlPatterns.size(), "Verify that urlPatterns contains exactly one element");
        assertInstanceOf(Set.class, urlPatterns, "Verify that urlPatterns is an instance of Set");
        assertEquals("loggingFilter", actualLoggingFilterResult.getFilterName(), "Verify that the filter name is 'loggingFilter'");
        assertEquals(1, actualLoggingFilterResult.getOrder(), "Verify that the order of the filter is 1");
        assertFalse(actualLoggingFilterResult.isMatchAfter(), "Verify that the filter does not match after");
        assertTrue(urlPatterns.contains("/*"), "Verify that urlPatterns contains the '/*' pattern");
        assertTrue(servletNames.isEmpty(), "Verify that servletNames is empty");
        assertTrue(servletRegistrationBeans.isEmpty(), "Verify that servletRegistrationBeans is empty");
        assertTrue(actualLoggingFilterResult.getInitParameters().isEmpty(), "Verify that the initialization parameters are empty");
        assertTrue(actualLoggingFilterResult.isAsyncSupported(), "Verify that async is supported");
        assertTrue(actualLoggingFilterResult.isEnabled(), "Verify that the filter is enabled");

    }
}
