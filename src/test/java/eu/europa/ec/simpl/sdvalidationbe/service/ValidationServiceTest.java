package eu.europa.ec.simpl.sdvalidationbe.service;

import org.apache.jena.rdf.model.Model;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ValidationServiceTest {
    private ValidationService validationService;

    @BeforeEach
    void setUp() {
        validationService = new ValidationService();
    }


    @Test
    void testValidateTtlFileInvalidEcosystem() throws IllegalStateException {
        MockMultipartFile ttlFile = new MockMultipartFile("ttlFile", "file.ttl", "text/plain", "".getBytes());
        MockMultipartFile shapeFile = new MockMultipartFile("shapeFile", "file.ttl", "text/plain", "".getBytes());

        assertThrows(IllegalStateException.class,
                () -> validationService.validateTtlFile(ttlFile, shapeFile, null),
                "Verify that IllegalStateException is thrown when validateTtlFile is called with null as the third argument"
        );

    }

    @Test
    void testValidateTtlFileIOException() throws IOException {
        MultipartFile ttlFile = mock(MultipartFile.class);
        MultipartFile shapeFile = mock(MultipartFile.class);

        InputStream shapeStream = mock(InputStream.class);


        when(ttlFile.getInputStream()).thenThrow(new IOException("Simulated IOException"));
        when(shapeFile.getInputStream()).thenReturn(shapeStream);


        String result = validationService.validateTtlFile(ttlFile, shapeFile, "example");


        assertNull(result, "Verify that the result is null");

        verify(ttlFile, times(1)).getInputStream();
        verify(shapeFile, times(0)).getInputStream();

        shapeStream.close();
        verify(shapeStream, times(1)).close();
    }


    @Test
    void testGetNsPrefixValidKey() {
        Model model = mock(Model.class);
        when(model.getNsPrefixMap()).thenReturn(Map.of("example", "http://simple.eu/"));

        String result = validationService.getNsPrefix(model, "example");
        assertEquals("http://simple.eu/", result, "Verify that the result matches the expected URL 'http://simple.eu/'");
    }

    @Test
    void testGetNsPrefixInvalidKey() {
        Model model = mock(Model.class);
        when(model.getNsPrefixMap()).thenReturn(Map.of());

        String result = validationService.getNsPrefix(model, "example");
        assertNull(result, "Verify that the result is null");
    }

    @Test
    void testIsValidEcosystemValidEcosystem() {
        assertTrue(validationService.isValidEcosystem("example-ecosystem"), "Verify that the ecosystem 'example-ecosystem' is valid");
    }

    @Test
    void testIsValidEcosystemInvalidEcosystem() {
        assertFalse(validationService.isValidEcosystem("invalid ecosystem!"), "Verify that the ecosystem 'invalid ecosystem!' is not valid");
    }

    @Test
    void testIsValidEcosystemNullEcosystem() {
        assertFalse(validationService.isValidEcosystem(null), "Verify that the ecosystem is not valid when it is null");
    }

}
