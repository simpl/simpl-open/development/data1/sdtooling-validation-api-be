package eu.europa.ec.simpl.sdvalidationbe.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;


class LoggingAspectTests {

    @InjectMocks
    private LoggingAspect loggingAspect;

    private MockedStatic<RequestContextHolder> mockStaticRequestContextHolder;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.openMocks(this);
        mockStaticRequestContextHolder = Mockito.mockStatic(RequestContextHolder.class);
    }

    @AfterEach
    void afterEach() {
        mockStaticRequestContextHolder.close();
    }

    /**
     * Method under test:
     * {@link LoggingAspect#trace(ProceedingJoinPoint, LogRequest)}
     */
    @Test
    void testTrace() throws Throwable {
        // Arrange
        ProceedingJoinPoint pjp = mock(ProceedingJoinPoint.class);
        when(pjp.getTarget()).thenReturn(LoggingAspectTests.class);
        when(pjp.proceed()).thenReturn("Proceed");

        // Act
        Object actualTraceResult = loggingAspect.trace(pjp, mock(LogRequest.class));

        assertEquals("Proceed", actualTraceResult, "Verify that the actual trace result is 'Proceed'");
    }

    /**
     * Method under test:
     * {@link LoggingAspect#logException(JoinPoint, LogRequest, Throwable)}
     */
    @Test
    void testLogException() {
        // Arrange
        JoinPoint joinPoint = mock(JoinPoint.class);
        when(joinPoint.getTarget()).thenReturn(LoggingAspectTests.class);
        LogRequest logRequest = mock(LogRequest.class);

        try (MockedStatic<LogManager> mockLogManager = Mockito.mockStatic(LogManager.class)) {
            Logger mockLogger = mock(Logger.class);
            when(LogManager.getLogger(Mockito.any(Class.class))).thenReturn(mockLogger);

            Throwable throwable = new Throwable("TestException");
            loggingAspect.logException(joinPoint, logRequest, throwable);

            assertEquals("TestException", throwable.getMessage(), "Only for coverage the not null exception");

        }

    }

    @Test
    void testLogBadRequest() {
        // Arrange
        JoinPoint joinPoint = mock(JoinPoint.class);
        when(joinPoint.getTarget()).thenReturn("Target");

        LogRequest logRequest = mock(LogRequest.class);
        when(logRequest.httpStatus()).thenReturn(HttpStatus.BAD_REQUEST);

        // Mock per RequestContextHolder e request/response
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("GET");
        request.setRequestURI("/test");
        request.setAttribute(RequestSizeFilter.REQUEST_SIZE_ATTRIBUTE, 1024);

        MockHttpServletResponse response = new MockHttpServletResponse();

        RequestAttributes requestAttributes = new ServletRequestAttributes(request, response);

        mockStaticRequestContextHolder.when(RequestContextHolder::getRequestAttributes).thenReturn(requestAttributes);

        try (MockedStatic<LogManager> mockLogManager = Mockito.mockStatic(LogManager.class)) {
            Logger mockLogger = mock(Logger.class);
            when(LogManager.getLogger(Mockito.any(Class.class))).thenReturn(mockLogger);

            loggingAspect.logException(joinPoint, logRequest, null);

            ArgumentCaptor<String> logCaptor = ArgumentCaptor.forClass(String.class);
            verify(mockLogger).error(logCaptor.capture());

            String logMessage = logCaptor.getValue();
            assertTrue(logMessage.contains("httpStatus=400"), "Message contains HTTP 400");
        }
    }

    @Test
    void testLogBadRequestWithoutRequestSize() {
        // Arrange
        JoinPoint joinPoint = mock(JoinPoint.class);
        when(joinPoint.getTarget()).thenReturn("Target");

        LogRequest logRequest = mock(LogRequest.class);
        when(logRequest.httpStatus()).thenReturn(HttpStatus.BAD_REQUEST);

        // Mock per RequestContextHolder e request/response
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("GET");
        request.setRequestURI("/test");

        MockHttpServletResponse response = new MockHttpServletResponse();

        RequestAttributes requestAttributes = new ServletRequestAttributes(request, response);

        mockStaticRequestContextHolder.when(RequestContextHolder::getRequestAttributes).thenReturn(requestAttributes);

        try (MockedStatic<LogManager> mockLogManager = Mockito.mockStatic(LogManager.class)) {
            Logger mockLogger = mock(Logger.class);
            when(LogManager.getLogger(Mockito.any(Class.class))).thenReturn(mockLogger);

            loggingAspect.logException(joinPoint, logRequest, null);

            ArgumentCaptor<String> logCaptor = ArgumentCaptor.forClass(String.class);
            verify(mockLogger).error(logCaptor.capture());

            String logMessage = logCaptor.getValue();
            assertTrue(logMessage.contains("httpStatus=400"), "Message contains HTTP 400");
        }
    }


}
