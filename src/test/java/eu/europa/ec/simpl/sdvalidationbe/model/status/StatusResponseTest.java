package eu.europa.ec.simpl.sdvalidationbe.model.status;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StatusResponseTest {

    @Test
    void testStatusResponseCreation() {
        String expectedStatus = "UP";
        String expectedVersion = "1.0.0";

        StatusResponse response = new StatusResponse(expectedStatus, expectedVersion);

        assertEquals(expectedStatus, response.getStatus(), "Verify that the response status matches the expected status");
        assertEquals(expectedVersion, response.getVersion(), "Verify that the response version matches the expected version");

    }

    @Test
    void testSetterMethods() {

        StatusResponse response = new StatusResponse("DOWN", "0.9.0");

        response.setStatus("UP");
        response.setVersion("1.0.1");

        assertEquals("UP", response.getStatus(), "Verify that the response status is 'UP'");
        assertEquals("1.0.1", response.getVersion(), "Verify that the response version is '1.0.1'");

    }

}
