package eu.europa.ec.simpl.sdvalidationbe.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StatusController.class)
class StatusControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private HealthEndpoint healthEndpoint;

    @Test
    void testGetStatusSuccess() throws Exception {
        Health mockHealth = Health.up().build();
        when(healthEndpoint.health()).thenReturn(mockHealth);

        mockMvc.perform(get("/status"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.version").value(Matchers.matchesRegex("\\d+\\.\\d+\\.\\d+")));

    }

    @Test
    void testGetStatusCheckIOException() throws Exception {
        Health mockHealth = Health.up().build();
        when(healthEndpoint.health()).thenReturn(mockHealth);

        try (MockedStatic<Files> filesMockedStatic = mockStatic(Files.class)) {
            filesMockedStatic.when(() -> Files.lines(any(Path.class)))
                    .thenThrow(new IOException("Test IOException"));

            mockMvc.perform(get("/status"))
                    .andExpect(status().isOk());
        }
    }

}
