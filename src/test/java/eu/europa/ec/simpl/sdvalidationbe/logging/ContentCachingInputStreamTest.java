package eu.europa.ec.simpl.sdvalidationbe.logging;

import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class ContentCachingInputStreamTest {

    private ServletInputStream servletInputStream;
    private CachedBodyHttpServletRequest cachedBodyHttpServletRequest;
    private ByteArrayOutputStream cachedContent;
    private ContentCachingInputStream contentCachingInputStream;

    @BeforeEach
    void setUp() {
        // Mocking dependencies
        servletInputStream = mock(ServletInputStream.class);
        cachedBodyHttpServletRequest = mock(CachedBodyHttpServletRequest.class);
        cachedContent = new ByteArrayOutputStream();

        // Initializing the class under test
        contentCachingInputStream = new ContentCachingInputStream(cachedBodyHttpServletRequest, servletInputStream, 1024, cachedContent);
    }

    @Test
    void testReadSingleByte() throws IOException {
        // Simulate reading a byte from the stream ASCII 'A'
        when(servletInputStream.read()).thenReturn(65);

        int result = contentCachingInputStream.read();

        assertEquals(65, result, "Verifying the byte read");
        assertEquals(1, cachedContent.size(), "'A' written to cache");
    }

    @Test
    void testReadTriggersHandleContentOverflow() throws IOException {
        int contentCacheLimit = 10;

        try (
                ContentCachingInputStream localContentCachingInputStream = new ContentCachingInputStream(cachedBodyHttpServletRequest, servletInputStream, contentCacheLimit, cachedContent);
        ) {
            for (int i = 0; i < contentCacheLimit; i++) {
                when(servletInputStream.read()).thenReturn(1);
                localContentCachingInputStream.read();
            }

            when(servletInputStream.read()).thenReturn(1);

            localContentCachingInputStream.read();

            verify(cachedBodyHttpServletRequest, times(1)).handleContentOverflow(contentCacheLimit);
        }
    }

    @Test
    void testReadByteArrayTriggersHandleContentOverflow() throws IOException {
        int contentCacheLimit = 2;

        byte[] buffer = new byte[5];

        // Simulate reading 3 bytes
        when(servletInputStream.read(buffer)).thenReturn(3);

        try (
                ContentCachingInputStream localContentCachingInputStream = new ContentCachingInputStream(cachedBodyHttpServletRequest, servletInputStream, contentCacheLimit, cachedContent);
        ) {
            for (int i = 0; i < contentCacheLimit; i++) {
                when(servletInputStream.read()).thenReturn(1);
                localContentCachingInputStream.read();
            }

            int result = localContentCachingInputStream.read(buffer);

            assertEquals(3, result, "Verify that the result is equal to 3");
            assertEquals(contentCacheLimit, cachedContent.size(), "Verify that cached content size matches the content cache limit");

        }
    }

    @Test
    void testReadStoresDataInCache() throws IOException {
        byte[] buffer = new byte[5];
        int off = 0;
        int len = buffer.length;

        when(servletInputStream.read(buffer, off, len)).thenReturn(len);

        int bytesRead = contentCachingInputStream.read(buffer, off, len);

        assertEquals(len, bytesRead, "Ensure the number of bytes read is correct");

        assertArrayEquals(buffer, cachedContent.toByteArray(), "Verify data is stored in the cache");
    }

    @Test
    void testReadLineStoresDataInCache() throws IOException {
        byte[] buffer = new byte[5];
        int off = 0;
        int len = buffer.length;

        when(servletInputStream.readLine(buffer, off, len)).thenReturn(len);

        int bytesRead = contentCachingInputStream.readLine(buffer, off, len);

        assertEquals(len, bytesRead, "Ensure the number of bytes read is correct");

        assertArrayEquals(buffer, cachedContent.toByteArray(), "Verify data is stored in the cache");
    }

    @Test
    void testReadByteArray() throws IOException {
        byte[] buffer = new byte[5];

        // Simulate reading 3 bytes
        when(servletInputStream.read(buffer)).thenReturn(3);

        int result = contentCachingInputStream.read(buffer);

        assertEquals(3, result, "Verify the number of bytes read");
        verify(cachedBodyHttpServletRequest, never()).handleContentOverflow(anyInt());
        assertEquals(3, cachedContent.size(), "Bytes written to cache");
    }

    @Test
    void testReadOverflow() {
        // Check if the overflow logic was triggered
        verify(cachedBodyHttpServletRequest, never()).handleContentOverflow(anyInt());
    }

    @Test
    void testIsFinished() {
        when(servletInputStream.isFinished()).thenReturn(true);

        assertTrue(contentCachingInputStream.isFinished(), "Verify that the content caching input stream is finished");

    }

    @Test
    void testIsReady() {
        when(servletInputStream.isReady()).thenReturn(true);

        assertTrue(contentCachingInputStream.isReady(), "Verify that the content caching input stream is ready");

    }

    @Test
    void testSetReadListener() {
        ReadListener readListener = mock(ReadListener.class);
        doNothing().when(servletInputStream).setReadListener(readListener);

        contentCachingInputStream.setReadListener(readListener);

        verify(servletInputStream).setReadListener(readListener);
    }
}
