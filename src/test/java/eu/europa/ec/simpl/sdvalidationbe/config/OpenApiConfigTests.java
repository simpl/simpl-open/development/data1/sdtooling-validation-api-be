package eu.europa.ec.simpl.sdvalidationbe.config;

import io.swagger.v3.oas.models.OpenAPI;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class OpenApiConfigTests {

    private final OpenApiConfig openApiConfig = new OpenApiConfig();

    @Test
    void testWithServers() {
        ReflectionTestUtils.setField(openApiConfig, "servers", List.of("https://api.example.com"));

        OpenAPI openAPI = openApiConfig.openAPI();

        assertNotNull(openAPI, "Verify that openAPI is not null");
        assertNotNull(openAPI.getInfo(), "Verify that openAPI info is not null");
        assertNotNull(openAPI.getSecurity(), "Verify that openAPI security is not null");
        assertFalse(openAPI.getServers().isEmpty(), "Verify that openAPI servers list is not empty");
        assertEquals("https://api.example.com", openAPI.getServers().get(0).getUrl(), "Verify that the server URL is 'https://api.example.com'");

    }

}
