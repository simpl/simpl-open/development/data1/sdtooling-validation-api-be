package eu.europa.ec.simpl.sdvalidationbe.util.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;


class CommonsMultipartFileTests {
    private CommonsMultipartFile multipartFile;
    private String fileName = "test.txt";
    private String contentType = "text/plain";
    private byte[] content = "This is a test file".getBytes();

    @BeforeEach
    void setUp() {
        multipartFile = new CommonsMultipartFile(fileName, contentType, content);
    }

    @Test
    void testGetName() {
        assertEquals(fileName, multipartFile.getName(), "fileName=multipartFileName");
    }

    @Test
    void testGetOriginalFilename() {
        assertEquals(fileName, multipartFile.getOriginalFilename(), "fileName=multipartOriginalFileName");
    }

    @Test
    void testGetContentType() {
        assertEquals(contentType, multipartFile.getContentType(), "contentType=multipartFileContentType");
    }

    @Test
    void testIsEmptyWhenContentIsNotEmpty() {
        assertFalse(multipartFile.isEmpty(), "multipartFile=isNotEmpty");
    }

    @Test
    void testIsEmptyWhenContentIsEmpty() {
        CommonsMultipartFile emptyMultipartFile = new CommonsMultipartFile(fileName, contentType, new byte[0]);
        assertTrue(emptyMultipartFile.isEmpty(), "multipartFile=isEmpty");
    }

    @Test
    void testGetSize() {
        assertEquals(content.length, multipartFile.getSize(), "contentLength=multipartFileSize");
    }

    @Test
    void testGetBytes() throws IOException {
        assertArrayEquals(content, multipartFile.getBytes(), "contentBytes=multipartFileBytes");
    }

    @Test
    void testGetInputStream() throws IOException {
        try (InputStream inputStream = multipartFile.getInputStream()) {
            assertNotNull(inputStream, "inputStram not null");
            assertInstanceOf(ByteArrayInputStream.class, inputStream, "inputStream instance of ByteArrayInputStream");

            byte[] readContent = new byte[content.length];
            assertEquals(content.length, inputStream.read(readContent), "contentLength=readContentLength");
            assertArrayEquals(content, readContent, "content=readContent");
        }
    }

    @Test
    void testGetResource() {
        Resource resource = multipartFile.getResource();
        assertNotNull(resource, "resource not null");
    }

    @Test
    void testTransferToFileNotSupported() {
        File dest = new File("test.txt");
        UnsupportedOperationException exception = assertThrows(UnsupportedOperationException.class, () -> {
            multipartFile.transferTo(dest);
        }, "throws UnsupportedOperationException");
        assertEquals("transferTo is not supported", exception.getMessage(), "exceptionMessage=transferTo is not supported");
    }

    @Test
    void testTransferToPath() throws Exception {
        MultipartFile mockedMultipartFile = mock(MultipartFile.class);

        Path dest = Path.of("dummy/path");

        doNothing().when(mockedMultipartFile).transferTo(dest);

        assertDoesNotThrow(() -> mockedMultipartFile.transferTo(dest), "not throws");
    }
}
