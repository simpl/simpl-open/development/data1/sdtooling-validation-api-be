package eu.europa.ec.simpl.sdvalidationbe;

import java.io.InputStream;

public final class TestSupport {

    private TestSupport() {
    }

    public static InputStream getResourceAsStream(String name) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        return classloader.getResourceAsStream(name);
    }

}
