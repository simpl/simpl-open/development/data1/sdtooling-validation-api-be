package eu.europa.ec.simpl.sdvalidationbe.logging;

import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.util.WebUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CachedBodyHttpServletRequestTests {

    private HttpServletRequest requestMock;
    private CachedBodyHttpServletRequest cachedRequest;

    @BeforeEach
    void setUp() throws IOException {
        requestMock = mock(HttpServletRequest.class);
        when(requestMock.getContentLength()).thenReturn(1024);
        try (MockServletInputStream mockInputStream = new MockServletInputStream("test body")) {
            when(requestMock.getInputStream()).thenReturn(mockInputStream);
        }
        when(requestMock.getCharacterEncoding()).thenReturn("UTF-8");

        cachedRequest = new CachedBodyHttpServletRequest(requestMock);
    }

    @Test
    void testGetInputStream() throws IOException {
        String expectedContent = "test body";
        int expectedBytesCount = expectedContent.getBytes().length;
        ServletInputStream inputStream = cachedRequest.getInputStream();
        ServletInputStream inputStream2 = cachedRequest.getInputStream();

        assertEquals(inputStream, inputStream2, "Equals inputStream");

        byte[] content = new byte[expectedContent.getBytes().length];
        int readBytes = inputStream.read(content);

        assertEquals(expectedBytesCount, readBytes, "readBytes=test body bytes");
        assertEquals(expectedContent, new String(content), "content=test body");
    }

    @Test
    void testGetCharacterEncoding() {
        assertEquals("UTF-8", cachedRequest.getCharacterEncoding(), "chached character=UTF-8");
    }

    @Test
    void testGetCharacterEncodingNull() {
        when(requestMock.getCharacterEncoding()).thenReturn(null);
        assertEquals(WebUtils.DEFAULT_CHARACTER_ENCODING, cachedRequest.getCharacterEncoding(), "Default character");
    }

    @Test
    void testGetParameter() {
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameter("param")).thenReturn("value");

        String param = cachedRequest.getParameter("param");
        assertEquals("value", param, "value=param");
    }

    @Test
    void testGetParameterMap() {
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(
                Map.of(
                        "param1", new String[]{"value1"},
                        "param2", new String[]{"value2"}
                )
        );

        Map<String, String[]> paramMap = cachedRequest.getParameterMap();
        assertTrue(paramMap.containsKey("param1"), "paramMap contains param1");
        assertArrayEquals(new String[]{"value1"}, paramMap.get("param1"), "param1=value1");
        assertTrue(paramMap.containsKey("param2"), "paramMap contains param2");
        assertArrayEquals(new String[]{"value2"}, paramMap.get("param2"), "param2=value2");
    }

    @Test
    void testGetContentAsByteArray() throws IOException {
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        cachedRequest.getInputStream();
        cachedRequest.getParameterNames();    // load the cached content
        byte[] content = cachedRequest.getContentAsByteArray();
        String result = new String(content);
        assertEquals("param=value", result, "param=value");
    }

    @Test
    void testConstructorWithContentCacheLimit() throws IOException {
        cachedRequest = new CachedBodyHttpServletRequest(requestMock, 2048);
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        cachedRequest.getInputStream();
        cachedRequest.getParameterNames();    // load the cached content

        byte[] content = cachedRequest.getContentAsByteArray();
        String result = new String(content);
        assertEquals("param=value", result, "resul=value");
    }

    @Test
    void testHandleContentOverflow() throws IOException {
        cachedRequest = new CachedBodyHttpServletRequest(requestMock, 128);
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        cachedRequest.getInputStream();
        cachedRequest.getParameterNames();    // load the cached content

        byte[] content = cachedRequest.getContentAsByteArray();
        String result = new String(content);
        assertEquals("param=value", result, "result=value");
    }

    @Test
    void testGetReader() throws IOException {
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        cachedRequest.getInputStream();
        cachedRequest.getParameterNames();    // load the cached content

        BufferedReader br = cachedRequest.getReader();
        BufferedReader br2 = cachedRequest.getReader();

        assertEquals(br, br2, "Same buffer content");
    }

    @Test
    void testGetParameterValues() throws IOException {
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        when(requestMock.getParameterValues(anyString())).thenReturn(new String[]{"value"});
        cachedRequest.getInputStream();

        String[] values = cachedRequest.getParameterValues("param");
        String[] values2 = cachedRequest.getParameterValues("param");

        assertNotNull(values, "value not null");
        assertNotNull(values2, "value2 not null");
        assertEquals(1, values.length, "value length 1");
        assertEquals(1, values2.length, "value2 length 1");
        assertEquals(values[0], values2[0], "value=value2");
    }

    // Custom mock ServletInputStream for testing purposes
    static class MockServletInputStream extends ServletInputStream {
        private final ByteArrayInputStream bais;

        public MockServletInputStream(String body) {
            this.bais = new ByteArrayInputStream(body.getBytes());
        }

        @Override
        public int read() throws IOException {
            return bais.read();
        }

        @Override
        public boolean isFinished() {
            return bais.available() == 0;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener readListener) {
            // No-op
        }
    }
}
