package eu.europa.ec.simpl.sdvalidationbe.logging;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ValidationApiConstantsTest {

    @Test
    void testInvalidEcosystemMessage() {
        assertEquals("Invalid ecosystem value", ValidationApiConstants.INVALID_ECOSYSTEM.getMessage(), "Verify that the error message matches the expected 'Invalid ecosystem value'");
    }

    @Test
    void testEnumConstantName() {
        assertEquals("INVALID_ECOSYSTEM", ValidationApiConstants.INVALID_ECOSYSTEM.name(), "Verify that the enum name matches 'INVALID_ECOSYSTEM'");
    }

    @Test
    void testEnumConstantValues() {
        ValidationApiConstants[] values = ValidationApiConstants.values();
        assertEquals(1, values.length, "Verify that the values array contains exactly one element");
        assertEquals(ValidationApiConstants.INVALID_ECOSYSTEM, values[0], "Verify that the first value in the array matches INVALID_ECOSYSTEM");
    }

}
