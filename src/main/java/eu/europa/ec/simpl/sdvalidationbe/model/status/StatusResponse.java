package eu.europa.ec.simpl.sdvalidationbe.model.status;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class StatusResponse {

    @JsonProperty("status")
    private String status;

    @JsonProperty("version")
    private String version;

    public StatusResponse(String status, String version) {
        this.status = status;
        this.version = version;
    }
}
