package eu.europa.ec.simpl.sdvalidationbe.logging;

import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.web.util.WebUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Map;

@Log4j2
public class CachedBodyHttpServletRequest extends HttpServletRequestWrapper {

    private static final int DEFAULT_CONTENT_LENGTH = 1024;

    private final ByteArrayOutputStream cachedContent;

    @Nullable
    private final Integer contentCacheLimit;

    @Nullable
    private ServletInputStream inputStream;

    @Nullable
    private BufferedReader reader;

    public CachedBodyHttpServletRequest(HttpServletRequest request) {
        super(request);
        int contentLength = request.getContentLength();
        if (contentLength < 0) {
            contentLength = DEFAULT_CONTENT_LENGTH;
        }
        this.cachedContent = new ByteArrayOutputStream(contentLength);
        this.contentCacheLimit = null;
    }

    public CachedBodyHttpServletRequest(HttpServletRequest request, int contentCacheLimit) {
        super(request);
        this.cachedContent = new ByteArrayOutputStream(contentCacheLimit);
        this.contentCacheLimit = contentCacheLimit;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (this.inputStream == null) {
            this.inputStream = new ContentCachingInputStream(this, getRequest().getInputStream(), contentCacheLimit, cachedContent);
        }
        return this.inputStream;
    }

    @Override
    public String getCharacterEncoding() {
        String enc = super.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        return enc;
    }

    @Override
    public BufferedReader getReader() throws IOException {
        if (this.reader == null) {
            this.reader = new BufferedReader(new InputStreamReader(getInputStream(), getCharacterEncoding()));
        }
        return this.reader;
    }

    @Override
    public String getParameter(String name) {
        if (this.cachedContent.size() == 0 && isFormPost()) {
            writeRequestParametersToCachedContent();
        }
        return super.getParameter(name);
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        if (this.cachedContent.size() == 0 && isFormPost()) {
            writeRequestParametersToCachedContent();
        }
        return super.getParameterMap();
    }

    @Override
    public Enumeration<String> getParameterNames() {
        if (this.cachedContent.size() == 0 && isFormPost()) {
            writeRequestParametersToCachedContent();
        }
        return super.getParameterNames();
    }

    @Override
    public String[] getParameterValues(String name) {
        if (this.cachedContent.size() == 0 && isFormPost()) {
            writeRequestParametersToCachedContent();
        }
        return super.getParameterValues(name);
    }

    private boolean isFormPost() {
        String contentType = getContentType();
        return (contentType != null && contentType.contains(MediaType.APPLICATION_FORM_URLENCODED_VALUE) && HttpMethod.POST.matches(getMethod()));
    }

    private void writeRequestParametersToCachedContent() {

        try {
            Map<String, String[]> form = super.getParameterMap();
            if (form.isEmpty()) {
                return;
            }

            String requestEncoding = getCharacterEncoding();
            writeEncodedParameters(form, requestEncoding);

        } catch (IOException ex) {
            throw new IllegalStateException("Failed to write request parameters to cached content", ex);
        }
    }

    private void writeEncodedParameters(Map<String, String[]> form, String encoding) throws IOException {
        boolean firstParam = true;

        for (Map.Entry<String, String[]> entry : form.entrySet()) {
            if (!firstParam) {
                cachedContent.write('&');
            }
            firstParam = false;

            writeEncoded(entry.getKey(), encoding);
            writeValues(entry.getValue(), encoding);
        }
    }

    private void writeValues(String[] values, String encoding) throws IOException {
        if (values == null || values.length == 0) {
            return;
        }

        cachedContent.write('=');
        boolean firstValue = true;

        for (String value : values) {
            if (!firstValue) {
                cachedContent.write('&');
            }
            firstValue = false;

            if (value != null) {
                writeEncoded(value, encoding);
            }
        }
    }

    private void writeEncoded(String value, String encoding) throws IOException {
        cachedContent.write(URLEncoder.encode(value, encoding).getBytes(StandardCharsets.UTF_8));
    }


    public byte[] getContentAsByteArray() {
        return this.cachedContent.toByteArray();
    }

    protected void handleContentOverflow(int contentCacheLimit) {
        // can be overridden by sub class
        log.warn("Content cache limit of {} exceeded", contentCacheLimit);
    }

}
