package eu.europa.ec.simpl.sdvalidationbe.logging;

import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;
import org.springframework.lang.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
class ContentCachingInputStream extends ServletInputStream {

    private final CachedBodyHttpServletRequest cachedBodyHttpServletRequest;
    private final ServletInputStream servletInputStream;
    private final Integer contentCacheLimit;
    private final ByteArrayOutputStream cachedContent;

    private boolean overflow;

    public ContentCachingInputStream(CachedBodyHttpServletRequest cachedBodyHttpServletRequest, ServletInputStream servletInputStream, Integer contentCacheLimit, ByteArrayOutputStream cachedContent) {
        this.cachedBodyHttpServletRequest = cachedBodyHttpServletRequest;
        this.servletInputStream = servletInputStream;
        this.contentCacheLimit = contentCacheLimit;
        this.cachedContent = cachedContent;
    }

    @Override
    public int read() throws IOException {
        int ch = this.servletInputStream.read();
        if (ch != -1 && !this.overflow) {
            if (contentCacheLimit != null && cachedContent.size() == contentCacheLimit) {
                this.overflow = true;
                cachedBodyHttpServletRequest.handleContentOverflow(contentCacheLimit);
            } else {
                cachedContent.write(ch);
            }
        }
        return ch;
    }

    @Override
    public int read(@NonNull byte[] b) throws IOException {
        int count = this.servletInputStream.read(b);
        writeToCache(b, 0, count);
        return count;
    }

    private void writeToCache(final byte[] b, final int off, int count) {
        if (!this.overflow && count > 0) {
            if (contentCacheLimit != null && count + cachedContent.size() > contentCacheLimit) {
                this.overflow = true;
                cachedContent.write(b, off, contentCacheLimit - cachedContent.size());
                cachedBodyHttpServletRequest.handleContentOverflow(contentCacheLimit);
                return;
            }
            cachedContent.write(b, off, count);
        }
    }

    @Override
    public int read(@NonNull final byte[] b, final int off, final int len) throws IOException {
        int count = this.servletInputStream.read(b, off, len);
        writeToCache(b, off, count);
        return count;
    }

    @Override
    public int readLine(final byte[] b, final int off, final int len) throws IOException {
        int count = this.servletInputStream.readLine(b, off, len);
        writeToCache(b, off, count);
        return count;
    }

    @Override
    public boolean isFinished() {
        return this.servletInputStream.isFinished();
    }

    @Override
    public boolean isReady() {
        return this.servletInputStream.isReady();
    }

    @Override
    public void setReadListener(ReadListener readListener) {
        this.servletInputStream.setReadListener(readListener);
    }
}
