package eu.europa.ec.simpl.sdvalidationbe.service;

import lombok.extern.log4j.Log4j2;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.topbraid.jenax.util.JenaUtil;
import org.topbraid.shacl.validation.ValidationUtil;
import org.topbraid.shacl.vocabulary.SH;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.regex.Pattern;

@Service
@Log4j2
public class ValidationService {

    private static final Pattern PATTERN = Pattern.compile("^[\\p{L}\\p{N}-_]+$");

    /**
     * Validate a ttl file with value against shape file with schema model
     *
     * @param ttlFile   ttl file
     * @param shapeFile shape file
     * @param ecosystem string indicates the ecosystem selected
     * @return Serialization for The JSON report Result
     */
    public String validateTtlFile(MultipartFile ttlFile, MultipartFile shapeFile, String ecosystem) {
        String method = "Method: validateTtlFile - ";
        log.info("{}{}", method, "Init");
        log.info("{} ecosystem {}", method, ecosystem);

        try (
                InputStream inputStreamTtlFile = ttlFile.getInputStream();
                InputStream inputStreamShapeFile = shapeFile.getInputStream();
                InputStream ttlFileInputStream = ttlFile.getInputStream();
                InputStream shapeFileStream = shapeFile.getInputStream()
        ) {
            // Read and log TTL file
            String inputTtlFile = new String(inputStreamTtlFile.readAllBytes(), StandardCharsets.UTF_8);
            log.info("{}processing TTL file content:\n {}", method, inputTtlFile);

            // Read and log SHAPE file
            String inputShapeFile = new String(inputStreamShapeFile.readAllBytes(), StandardCharsets.UTF_8);
            log.info("{}processing SHAPE file content:\n {}", method, inputShapeFile);

            // Load models
            Model dataModel = JenaUtil.createDefaultModel();
            dataModel.read(ttlFileInputStream, null, RDFFormat.TTL.getLang().getName());

            Model shapeModel = JenaUtil.createDefaultModel();
            shapeModel.read(shapeFileStream, null, RDFFormat.TURTLE.getLang().getName());

            // Extract namespace prefix
            String uri = getNsPrefix(dataModel, ecosystem);
            if (uri == null) {
                log.error("{}no NS prefix found in data model for ecosystem '{}'", method, ecosystem);
                throw new IllegalStateException("No NS prefix found in data model for ecosystem '" + ecosystem + "'");
            }

            // Validation
            Resource reportResource = ValidationUtil.validateModel(dataModel, shapeModel, true);
            boolean conforms = reportResource.getProperty(SH.conforms).getBoolean();
            log.info("{}conforms={}", method, conforms);

            // Validation results
            String result = "";
            if (!conforms) {
                result = toString(reportResource);
            }

            log.info("{}returning\n {}", method, result);
            return result;

        } catch (IOException e) {
            log.error("{}failed cause IOException:{}", method, e.getMessage());
            return null;
        }

    }

    public String toString(Resource reportResource) {
        StringWriter stringWriter = new StringWriter();
        RDFDataMgr.write(stringWriter, reportResource.getModel(), RDFFormat.JSONLD);
        return stringWriter.toString();
    }

    public String getNsPrefix(Model model, String key) {
        Map<String, String> prefixMap = model.getNsPrefixMap();
        if (prefixMap.containsKey(key)) {
            return prefixMap.get(key);
        }
        return null;
    }

    public boolean isValidEcosystem(String ecosystem) {
        if (ecosystem == null || ecosystem.isEmpty()) {
            return false;
        }
        return PATTERN.matcher(ecosystem).matches();
    }

}
