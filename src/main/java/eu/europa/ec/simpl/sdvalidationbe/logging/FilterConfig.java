package eu.europa.ec.simpl.sdvalidationbe.logging;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean<RequestSizeFilter> loggingFilter(){
        FilterRegistrationBean<RequestSizeFilter> registrationBean = new FilterRegistrationBean<>();

        registrationBean.setFilter(new RequestSizeFilter());

        // Apply the filter to all URLs
        registrationBean.addUrlPatterns("/*");

        // Set the execution order (optional)
        registrationBean.setOrder(1);


        return registrationBean;
    }
}
