package eu.europa.ec.simpl.sdvalidationbe.logging;

import eu.simpl.types.HttpLogMessage;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Objects;

import static eu.simpl.MessageBuilder.buildMessage;

/**
 * Aspect for logging execution of service and repository Spring components.
 */
@Aspect
@Component
public class LoggingAspect {

    @Around("@annotation(logRequest)")
    public Object trace(ProceedingJoinPoint pjp, LogRequest logRequest) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object returnValue = pjp.proceed();
        logObject(pjp, logRequest, null, startTime);
        return returnValue;
    }

    @AfterThrowing(pointcut = "@annotation(logRequest)", throwing = "ex")
    public void logException(JoinPoint joinPoint, LogRequest logRequest, Throwable ex) {
        logObject(joinPoint, logRequest, ex, System.currentTimeMillis());
    }

    private static void logObject(JoinPoint joinPoint, LogRequest logRequest, Throwable ex, long startTime) {

        if (RequestContextHolder.getRequestAttributes() == null) {
            // Skip logging if no request context is available
            return;
        }

        Class<?> targetClass = joinPoint.getTarget().getClass();

        // Logger is created dynamically based on the target class
        Logger logger = LogManager.getLogger(targetClass);

        // Log exception if present
        if (ex != null) {
            logger.error("Exception occurred: {}", (Object) ex);
            return;
        }

        // Get HTTP request and response objects
        HttpServletRequest request = getHttpServletRequest();
        Integer requestSize = (Integer) request.getAttribute(RequestSizeFilter.REQUEST_SIZE_ATTRIBUTE);

        // Calculate elapsed time
        long elapsedTime = System.currentTimeMillis() - startTime;

        // Prepare log message details
        String msg = buildHttpRequestMessage(request);

        String httpRequestSize;
        if (requestSize != null && requestSize != -1) {
            httpRequestSize = requestSize + " bytes";
        } else {
            httpRequestSize = "N/A";
        }

        // Set user info, if applicable
        String user = "ANONYMOUS";

        // Log the appropriate message based on status
        HttpStatus status = logRequest.httpStatus();
        HttpLogMessage logMessage = buildHttpLogMessage(msg, status, httpRequestSize, elapsedTime, user);
        logMessage(logMessage, status, logger);

    }

    private static void logMessage(HttpLogMessage logMessage, HttpStatus status, Logger logger) {
        String message = String.valueOf(buildMessage(logMessage));
        if (status == HttpStatus.OK) {
            logger.info(message);
        } else {
            logger.error(message);
        }
    }

    private static String buildHttpRequestMessage(HttpServletRequest request) {
        return "HTTP request [" + request.getMethod() + "] " + request.getRequestURL();
    }

    private static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
                .getRequest();
    }

    private static HttpLogMessage buildHttpLogMessage(String msg, HttpStatus status, String requestSize, long elapsedTime, String user) {
        return HttpLogMessage.builder()
                .msg(msg)
                .httpStatus(String.valueOf(status.value()))
                .httpRequestSize(requestSize)
                .httpExecutionTime(elapsedTime + " ms")
                .user(user)
                .build();
    }
}
