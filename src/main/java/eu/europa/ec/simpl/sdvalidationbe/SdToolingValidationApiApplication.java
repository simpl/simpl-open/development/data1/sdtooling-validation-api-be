package eu.europa.ec.simpl.sdvalidationbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "eu.europa.ec.simpl.sdvalidationbe")
public class SdToolingValidationApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(SdToolingValidationApiApplication.class, args);
    }
}
