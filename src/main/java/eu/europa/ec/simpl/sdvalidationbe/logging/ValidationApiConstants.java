package eu.europa.ec.simpl.sdvalidationbe.logging;

import lombok.Getter;

@Getter
public enum ValidationApiConstants {
    INVALID_ECOSYSTEM("Invalid ecosystem value");

    private final String message;

    ValidationApiConstants(String message) {
        this.message = message;
    }

}
