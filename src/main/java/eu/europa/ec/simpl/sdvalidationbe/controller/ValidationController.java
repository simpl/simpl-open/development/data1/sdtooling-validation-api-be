package eu.europa.ec.simpl.sdvalidationbe.controller;

import eu.europa.ec.simpl.sdvalidationbe.logging.LogRequest;
import eu.europa.ec.simpl.sdvalidationbe.logging.ValidationApiConstants;
import eu.europa.ec.simpl.sdvalidationbe.service.ValidationService;
import eu.europa.ec.simpl.sdvalidationbe.util.web.CommonsMultipartFile;
import lombok.extern.log4j.Log4j2;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Main entry point for REST calls. Provides access to pre-defined files as well as a Turtle to JSON preprocessor for the frontend.
 */
@RestController
@Log4j2
public class ValidationController {

    private final ValidationService validationService;

    public ValidationController(ValidationService validationService) {
        this.validationService = validationService;
    }

    /**
     * A GET request method that validates data graph and shacl shape file
     *
     * @param ttlFile   ttl file
     * @param shapeFile shape file
     * @param ecosystem string indicates the ecosystem selected
     * @return serialization of the validation report for the validation of data graph against a shapes graph .
     */
    @PostMapping("/validate-ttl")
    @LogRequest
    public ResponseEntity<String> validateTTL(
            @RequestParam("ttl-file") MultipartFile ttlFile,
            @RequestParam("shape-file") MultipartFile shapeFile,
            String ecosystem
    ) {
        if (!validationService.isValidEcosystem(ecosystem)) {
            return ResponseEntity.badRequest().body(ValidationApiConstants.INVALID_ECOSYSTEM.getMessage());
        }
        String serializedJsonResponseReport = validationService.validateTtlFile(ttlFile, shapeFile, ecosystem);
        return new ResponseEntity<>(serializedJsonResponseReport, HttpStatus.OK);
    }

    /**
     * A GET request method that validates json ld and shacl shape file
     *
     * @param jsonldFile jsonld file
     * @param shapeFile  shape file
     * @param ecosystem  string indicates the ecosystem selected
     * @return serialization of the validation report for the validation of data graph against a shapes graph .
     */
    @PostMapping("/validate-jsonld")
    @LogRequest
    public ResponseEntity<String> validateJson(
            @RequestParam("jsonld-file") MultipartFile jsonldFile,
            @RequestParam("shape-file") MultipartFile shapeFile,
            @RequestParam("ecosystem") String ecosystem
    ) throws IOException {
        if (!validationService.isValidEcosystem(ecosystem)) {
            return ResponseEntity.badRequest().body(ValidationApiConstants.INVALID_ECOSYSTEM.getMessage());
        }
        // Read the content of the MultipartFile JSON-LD
        Model model = ModelFactory.createDefaultModel();
        try (InputStream inputStream = jsonldFile.getInputStream()) {
            model.read(inputStream, null, "JSON-LD");
        }
        // Write the RDF model in Turtle format to an output stream
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        RDFDataMgr.write(outputStream, model, RDFLanguages.TTL);

        // Create a new MultipartFile from the ByteArrayOutputStream
        MultipartFile ttlFile = new CommonsMultipartFile("converted.ttl", "application/x-turtle", outputStream.toByteArray());

        String serializedJsonResponseReport = validationService.validateTtlFile(ttlFile, shapeFile, ecosystem);

        return new ResponseEntity<>(serializedJsonResponseReport, HttpStatus.OK);
    }

}
