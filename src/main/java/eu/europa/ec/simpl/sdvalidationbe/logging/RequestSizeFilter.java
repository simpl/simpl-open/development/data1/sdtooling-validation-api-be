package eu.europa.ec.simpl.sdvalidationbe.logging;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;

public class RequestSizeFilter implements Filter {

    public static final String REQUEST_SIZE_ATTRIBUTE = "requestSize";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        CachedBodyHttpServletRequest wrappedRequest = new CachedBodyHttpServletRequest((HttpServletRequest) request);
        int contentLength = wrappedRequest.getContentLength();
        wrappedRequest.setAttribute(REQUEST_SIZE_ATTRIBUTE, contentLength);
        chain.doFilter(wrappedRequest, response);
    }
}

