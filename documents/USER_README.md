# SD Validation API

Following the API description implemented:
[OPEN API description](openapi.json)

```json
{
  "openapi": "3.0.1",
  "info": {
    "title": "SD Tooling Validation API",
    "description": "OpenApi documentation for the SD Tooling Validation Api",
    "version": "1.0"
  },accessKeyaccessKey
  "servers": [
    {
      "url": "http://localhost:8085",
      "description": "Generated server url"
    }
  ],
  "paths": {
    "/validate-ttl": {
      "post": {
        "tags": [
          "validation-controller"
        ],
        "operationId": "validateTTL",
        "parameters": [
          {
            "name": "ecosystem",
            "in": "query",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "required": [
                  "shape-file",
                  "ttl-file"
                ],
                "type": "object",
                "properties": {
                  "ttl-file": {
                    "type": "string",
                    "format": "binary"
                  },
                  "shape-file": {
                    "type": "string",
                    "format": "binary"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "*/*": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/validate-jsonld": {
      "post": {
        "tags": [
          "validation-controller"
        ],
        "operationId": "validateJson",
        "parameters": [
          {
            "name": "ecosystem",
            "in": "query",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "required": [
                  "jsonld-file",
                  "shape-file"
                ],
                "type": "object",
                "properties": {
                  "jsonld-file": {
                    "type": "string",
                    "format": "binary"
                  },
                  "shape-file": {
                    "type": "string",
                    "format": "binary"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "*/*": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    
  }
}