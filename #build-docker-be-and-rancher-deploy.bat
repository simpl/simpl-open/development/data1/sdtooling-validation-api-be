@ECHO off

set servicename=sdtooling-validation-api-be

ECHO ============================
ECHO SERVICE NAME : %servicename%
ECHO ============================

ECHO ============================
ECHO BUILD BE
ECHO ============================

call mvn clean package -DskipTests

ECHO ============================
ECHO DOCKER LOGIN
ECHO ============================

call docker login code.europa.eu:4567

ECHO ============================
ECHO DOCKER BUILD
ECHO ============================

call docker build -t code.europa.eu:4567/simpl/simpl-open/development/data1/%servicename% .

ECHO ============================
ECHO DOCKER PUSH
ECHO ============================

call docker push code.europa.eu:4567/simpl/simpl-open/development/data1/%servicename%

ECHO ============================
ECHO GO TO HELM DIR
ECHO ============================

call cd deployment\sd\helm\sd-creation-wizard\

ECHO ============================
ECHO RANCHER UNINSTALL
ECHO ============================

call helm uninstall sd

ECHO ============================
ECHO RANCHER INSTALL
ECHO ============================

call helm install sd .

ECHO ============================
ECHO RETURN TO ROOT DIR
ECHO ============================

call cd ..\..\..\..\

ECHO ============================
ECHO ALL COMMAND COMPLETED
ECHO ============================

EXIT /B